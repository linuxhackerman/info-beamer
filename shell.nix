with import <nixpkgs> {};
mkShell {
  packages = [ xorg.libXi xorg.libXinerama xorg.libXxf86vm xorg.libX11 xorg.libXrandr xorg.libXcursor glfw glew lua5_1 libdevil ffmpeg libevent ftgl freetype ];
}
